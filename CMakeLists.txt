cmake_minimum_required(VERSION 3.8)
project(ParaViewJupyter)

find_package(ParaView REQUIRED)

option(BUILD_SHARED_LIBS "Build shared libraries" ON)

include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")

set("_paraview_plugin_default_${CMAKE_PROJECT_NAME}" ON)
paraview_plugin_scan(
  PLUGIN_FILES      "${CMAKE_CURRENT_SOURCE_DIR}/PVKernelPlugin/paraview.plugin"
  PROVIDES_PLUGINS  plugins
  REQUIRES_MODULES  required_modules)

foreach (module IN LISTS required_modules)
  if (NOT TARGET "${module}")
    message("Missing required module: ${module}")
    return ()
  endif ()
endforeach ()

paraview_plugin_build(
  RUNTIME_DESTINATION "${CMAKE_INSTALL_BINDIR}"
  LIBRARY_DESTINATION "${CMAKE_INSTALL_LIBDIR}"
  LIBRARY_SUBDIRECTORY "${PARAVIEW_PLUGIN_SUBDIR}"
  PLUGINS ${plugins})

# Find ParaView executable
# This program is launched at kernel start.
find_program(PARAVIEW_EXECUTABLE NAMES "paraview" PATHS "${ParaView_DIR}/bin" DOC "Path to ParaView")

# Find env executable.
# This is needed to pass the notebook config file to the kernel at runtime.
# In the kernel.json file, the ${connexion_file} var is replaced at runtime with the path of the
# actual connection file. But this replacement is done only in the `argv` list, not in the `env` one.
# Moreover, first item in the `argv' list should be the executable.
# See kernel.json.in
find_program(ENV_EXECUTABLE NAMES "env" DOC "Path to the `env` program")

set(KERNEL_INSTALL_PATH "~/.local/share/jupyter/kernels" CACHE PATH "Path where to install the kernel")
set(PLUGIN_INSTALL_PATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}/${PARAVIEW_PLUGIN_SUBDIR}/${PROJECT_NAME}")

configure_file(kernel.json.in ${CMAKE_BINARY_DIR}/kernel.json)
install(FILES ${CMAKE_BINARY_DIR}/kernel.json DESTINATION ${KERNEL_INSTALL_PATH}/ParaView)
