
#include "pqJupyterPluginTraceReaction.h"

#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>

#include <QApplication>
#include <QMainWindow>
#include <QMenuBar>
#include <QStyle>

#include <vtkSMTrace.h>

#include "PVInterpreter.h"

class pqJupyterPluginTraceReaction::pqInternals
{
public:
  QPointer<QAction> standardTraceAction;
  QString extraLines;

  pqInternals() = default;

  void findStandardTraceAction()
  {
    if (!this->standardTraceAction)
    {
      QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
      auto bar = mainWindow->menuBar();
      auto menus = bar->findChildren<QMenu*>();
      for (auto m : menus)
      {
        if (m->objectName() == QString("menuTools"))
        {
          for (auto a : m->actions())
          {
            if (a->objectName() == QString("actionToolsStartStopTrace"))
            {
              this->standardTraceAction = a;
            }
          }
        }
      }
    }
  }
};

//-----------------------------------------------------------------------------
pqJupyterPluginTraceReaction::pqJupyterPluginTraceReaction(QAction* p)
  : Superclass(p)
  , Internals(new pqInternals)
{
  auto coreApp = pqApplicationCore::instance();
  QObject::connect(coreApp, &pqApplicationCore::clientEnvironmentDone, this,
    &pqJupyterPluginTraceReaction::initialize);

  this->updateAction();
}

// Cannot be '= default' because member QScopedPointer<pqInternals> uses forward declared pqInternals.
// see https://doc.qt.io/qt-5/qscopedpointer.html#forward-declared-pointers
//-----------------------------------------------------------------------------
pqJupyterPluginTraceReaction::~pqJupyterPluginTraceReaction() {}

//-----------------------------------------------------------------------------
void pqJupyterPluginTraceReaction::initialize()
{
  this->Internals->findStandardTraceAction();
  // watch changed signal and not triggered to be sure the reaction already handled the trigger.
  this->connect(this->Internals->standardTraceAction, &QAction::triggered, this,
    &pqJupyterPluginTraceReaction::onStandardAction);

  // initialize python interpreter
  auto interpreter = dynamic_cast<PVInterpreter*>(&xeus::get_interpreter());
  interpreter->initialize_python();
}

//-----------------------------------------------------------------------------
void pqJupyterPluginTraceReaction::onStandardAction()
{
  auto action = this->parentAction();
  bool traceStarted = vtkSMTrace::GetActiveTracer() != nullptr;
  action->setEnabled(!traceStarted);
}

//-----------------------------------------------------------------------------
void pqJupyterPluginTraceReaction::updateAction()
{
  auto action = this->parentAction();
  bool traceStarted = vtkSMTrace::GetActiveTracer() != nullptr;
  if (traceStarted)
  {
    QIcon icon = qApp->style()->standardIcon(QStyle::SP_ArrowRight);
    action->setIcon(icon);
    action->setToolTip("Send Trace to Notebook");
  }
  else
  {
    QIcon icon = qApp->style()->standardIcon(QStyle::SP_DialogNoButton);
    action->setIcon(icon);
    action->setToolTip("Record Trace");
  }
}

//-----------------------------------------------------------------------------
void pqJupyterPluginTraceReaction::onTriggered()
{
  auto tracer = vtkSMTrace::GetActiveTracer();
  this->Internals->standardTraceAction->setEnabled(tracer);

  if (tracer)
  {
    auto trace = vtkSMTrace::StopTrace();
    QString traceString(trace.c_str());
    traceString.remove(this->Internals->extraLines);
    QStringList traceLines = traceString.split("\n");

    // remove empty lines at begining of trace
    while (!traceLines.isEmpty() && traceLines.first().trimmed().isEmpty())
    {
      traceLines.removeFirst();
    }

    // remove commented lines at the end of trace. See smtrace.py _stop_trace_internal
    while (!traceLines.isEmpty() && traceLines.last().startsWith("#"))
    {
      traceLines.removeLast();
    }

    if (!traceLines.isEmpty())
    {
      // escape backslashes as '\n' should appears in resulting js code.
      traceString = traceLines.join("\\n");
      traceString.append("\\n\\nRender()");

      auto interpreter = dynamic_cast<PVInterpreter*>(&xeus::get_interpreter());
      interpreter->push_to_new_cell(traceString.toStdString());
    }
  }
  else
  {
    vtkSMTrace::StartTrace();
    tracer = vtkSMTrace::GetActiveTracer();
    tracer->SetPropertiesToTraceOnCreate(vtkSMTrace::RECORD_USER_MODIFIED_PROPERTIES);
    // smTrace always adds unneeded lines.
    // see smtrace.py::_start_trace_internal
    this->Internals->extraLines = QString(tracer->GetCurrentTrace().c_str());
  }

  this->updateAction();
}
