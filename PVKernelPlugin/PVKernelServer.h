#ifndef PVKernelServer_h
#define PVKernelServer_h

// xeus includes
#include <xeus/xserver_zmq.hpp>

class QTimer;

namespace xeus
{
class xconfiguration;
}

class PVKernelServer : public xeus::xserver_zmq
{

public:
  PVKernelServer(zmq::context_t& context, const xeus::xconfiguration& config);

  virtual ~PVKernelServer();

protected:
  // Starts the kernel server
  void start_impl(zmq::multipart_t& message) override;
  // Stops the kernel server
  void stop_impl() override;

  // Socket notifier for stdin socket continuously generates signals
  // on Windows and on some Linux distributions, which would cause 100% CPU
  // usage even when the application is idle.
  // It is not clear why stdin socket behaves like this, but using a timer
  // to check for inputs at regular intervals solves the issue.
  // Info from
  // https://github.com/Slicer/SlicerJupyter/blob/1a316a2e57b62b191568bebbd5092da2262a501a/JupyterKernel/xSlicerServer.h
  QTimer* m_pollTimer;
};

std::unique_ptr<xeus::xserver> make_PVKernelServer(
  zmq::context_t& context, const xeus::xconfiguration& config);

#endif
